import pickle

import pandas as PD
import sklearn.ensemble as ENSEMBLE
from keras.layers import Dense, Dropout
from keras.models import Sequential
from pandas import DataFrame

import dataparser
import util

# LOAD THE TRAINING DATA
#Iterate over each piece of the data set
dftrainraw = PD.read_csv(util.TRAIN_FILE, chunksize=util.BATCHSIZE, dtype=str, index_col='ncodpers')

c = 0
trainx = DataFrame()
trainy = DataFrame()
for part in dftrainraw:
    c += 1
    print  "Chunk ", c, " ", part.shape
    s, t = dataparser.splitXY(part)
    s = dataparser.processChunk(s)
    assert ( s.isnull().any().any() == False)
    trainx = PD.concat([trainx, s], ignore_index=True)
    trainy = PD.concat([trainy, t], ignore_index=True)

print "===============\n"
print "Features Used:\n ", len(dataparser.KEYS_FULL)
trainx = dataparser.postprocess(trainx)
trainx = dataparser.multishape(trainx, dataparser.KEYS_FULL)

print "Normalizing"
trainx = dataparser.normalize(trainx)

#==========================
#Training phase
#===========================
#Create the model


def mmodel(insize, outsize):
    model = Sequential()
    model.add(Dense(insize, input_dim=insize, init='uniform', activation='linear'))
    #Add hidden layers layers
    for i in xrange(1, util.NUM_LAYERS):
        model.add(Dense(util.NUM_NEURONS, init='uniform', activation='relu'))
        model.add(Dropout(util.RATIO_DROP))
    model.add(Dense(outsize, init='uniform', activation='sigmoid'))
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

    return model

#Create a NN for predicting each product line
print "Starting training sessions"
s = util.tic()
ann = mmodel(trainx.shape[1], trainy.shape[1])
ann.fit(trainx.values, trainy.values, batch_size=10000, verbose=1, nb_epoch=util.NUM_EPOCHS, validation_split=util.RATIO_VALIDATION)
print "Training ANN took ", util.toc(s), " seconds"
s = util.tic()
forest = ENSEMBLE.RandomForestClassifier(util.NUM_FORESTS, "gini")
forest.fit(trainx.values, trainy.values)
print "Training on forest took ", util.toc(s), " seconds"

print "Saving model to file ", util.STOREFILE
# Saving the objects:
with open(util.STOREFILE, 'w') as f:  # Python 3: open(..., 'wb')
    pickle.dump([ann, forest, trainy.columns], f)

print "Done"