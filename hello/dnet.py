"""
@author Christopher Aprea
@company Circadence
@date August 2016
"""

import os
import numpy as np
import keras
import os

from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Convolution2D, MaxPooling2D
from keras.utils import np_utils as kutils


import numpy as NP
import time as TIME


batch_size = 128
nb_classes = 3
nb_epoch = 120

# input image dimensions


#DFILE = '/home/ca/data/pima-indians-diabetes.data'
DFILE = 'irisnum.data'
NUMFEATURES = 4


def getData(dataFile, numFeatures):
    print 'trying to get data'
    # load pima indians dataset
    dataset = NP.loadtxt(dataFile, delimiter=",")
    # split into input (X) and output (Y) variables

    numholdout = int(dataset.shape[0] * 0.10) #Number of rows to hold out for validation
    X_train = dataset[0:-numholdout, 0:numFeatures]
    y_train = dataset[0:-numholdout, numFeatures]
    X_test = dataset[numholdout:, 0:numFeatures]
    y_test = dataset[numholdout:, numFeatures]
    return (X_train, y_train), (X_test, y_test)

def convertLabel(data):
    mdata = kutils.to_categorical(data, nb_classes) # for use with categorical crossentropy
    return mdata

def preprocess(data):
    mdata = data.astype('float32')
    #mdata /= 255

    #Normalize the data between [0,1]
    normed = mdata #/ mdata.max(axis=0)

    return normed

def createModelFF():
    model = Sequential()

    model.add(Dense(1284, input_dim=NUMFEATURES, init='uniform', activation='relu'))

    model.add(Dense(100, init='uniform', activation='relu'))
    model.add(Dense(100, init='uniform', activation='relu'))
    model.add(Dense(100, init='uniform', activation='relu'))

    model.add(Dense(228))
    model.add(Activation('relu'))
    model.add(Dense(228))
    model.add(Dense(nb_classes))
    model.add(Activation('softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    print(model.summary())
    return model

def train(model, x, y, valx, valy):
    TIME.sleep(0.1)
    model.fit(x, y, batch_size=500, nb_epoch=nb_epoch, verbose=1, validation_data=(valx, valy))
    TIME.sleep(0.1)

    score = model.evaluate(valx, valy, verbose=0)
    print('Test score:', score[0])
    print('Test accuracy:', score[1])

def main():
    print "Starting"
    (x, y), (xtest, ytest) = getData(DFILE, NUMFEATURES)
    print "Data finished"
    y = convertLabel(y)
    ytest = convertLabel(ytest)
    print "Processing..."
    x = preprocess(x)
    xtest = preprocess(xtest)
    net = createModelFF()

    train(net, x, y, xtest, ytest)

    print "done"

'''
    ta = np.array([5, 121, 72, 23, 112, 26.2, 0.245, 30]).reshape(1,8)  #0
    tb = np.array([1, 126, 60, 0, 0, 30.1, 0.349, 47]).reshape(1,8)     #1
    tc = np.array([1, 93, 70, 31, 0, 30.4, 0.315, 23]).reshape(1,8)   #0
'''



if (__name__ == "__main__"):
    main()


