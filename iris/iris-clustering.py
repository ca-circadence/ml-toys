
# taken from http://scikit-learn.org/stable/auto_examples/cluster/plot_cluster_iris.html
# Modified for documentation by Jaques Grobler
# License: BSD 3 clause

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy
import pandas as PD
from pandas import DataFrame

from sklearn.cluster import KMeans
from sklearn import datasets

np.random.seed(5)

from sklearn import cluster, datasets
iris = datasets.load_iris()
kx_iris = iris.data
ky_iris = iris.target

k = cluster.KMeans(n_clusters=3)
k.fit(kx_iris)


#=================================


x = PD.read_csv('iris.data', names=['m1','m2','m3','m4','lbl' ] )
y = x.pop('lbl')

k = cluster.KMeans(n_clusters=3, verbose=1)
k.fit(x.values)

print k.predict([[6.2,3.4,5.4,2.3]])
print k.predict([[5.9,3,5.1,1.8]])
print k.predict([[5.0,2.0,3.5,1.0]])
print k.predict([[5.1,3.5,1.4,0.3]])