




from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers.embeddings import Embedding
from keras.preprocessing import sequence
import numpy
import pandas
import seaborn
import datetime
from pandas import DataFrame
from sklearn.model_selection import train_test_split

def display(data: DataFrame):
    seaborn.tsplot(time="Month", value="Passengers", unit='subject', data=data, interpolate=True)
    #seaborn.lmplot(x="Month", y="Passengers", data=data, palette="muted", size=10, fit_reg=True)
    seaborn.plt.show()

def learn(data):
    #Define the data sets
    top_words = 5000
    x_train, x_test, y_train, y_test = train_test_split(data['Month'].values, data['Passengers'].values)

    # truncate and pad input sequences
    maxl = 50

    x_train = sequence.pad_sequences(x_train, maxlen=maxl)
    x_test = sequence.pad_sequences(x_test, maxlen=maxl)

    # create the model
    model = Sequential()
    model.add(LSTM(100))
    model.add(Dense(1, activation='sigmoid'))
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    print(model.summary())
    model.fit(x_train, y_train, validation_data=(x_test, y_test), epochs=3, batch_size=64)


    # Final evaluation of the model
    scores = model.evaluate(x_test, y_test, verbose=0)
    print("Accuracy: %.2f%%" % (scores[1]*100))

def main():
    # fix random seed for reproducibility
    numpy.random.seed(7)

    data_gammas = seaborn.load_dataset("gammas")
    train = pandas.read_csv('alldata.csv')

    train['Month'] = train['Month'].apply(pandas.to_datetime)
    train['Month'] = train['Month'].apply(datetime.datetime.timestamp)
    train['subject'] = 0
    #train['Month'] += 500000000000


    learn(train)

if __name__ == "__main__":
    main()