import numpy as np
import pandas as PD

from pandas import DataFrame

'''
VERSION3,
'''
KEYS_DROPPED = ['cod_prov', 'tipodom']
KEYS_NORMED = ['age', 'renta', 'antiguedad']
KEYS_SYMBOLIC = ['segmento', 'nomprov', 'ind_empleado', 'tiprel_1mes','pais_residencia', 'canal_entrada']
KEYS_DATES = ['fecha_alta', 'fecha_dato', 'ult_fec_cli_1t']

# Get products columns
KEYS_BINARY = ['sexo', 'ind_nuevo', 'indresi', 'indext', 'indfall', 'ind_actividad_cliente', 'conyuemp', 'indrel']
KEYS_PRODUCTS = ['ind_ahor_fin_ult1', 'ind_aval_fin_ult1', 'ind_cco_fin_ult1', 'ind_cder_fin_ult1',
                 'ind_cno_fin_ult1', 'ind_ctju_fin_ult1', 'ind_ctma_fin_ult1', 'ind_ctop_fin_ult1',
                 'ind_ctpp_fin_ult1', 'ind_deco_fin_ult1', 'ind_deme_fin_ult1', 'ind_dela_fin_ult1',
                 'ind_ecue_fin_ult1', 'ind_fond_fin_ult1', 'ind_hip_fin_ult1', 'ind_plan_fin_ult1',
                 'ind_pres_fin_ult1', 'ind_reca_fin_ult1', 'ind_tjcr_fin_ult1', 'ind_valo_fin_ult1',
                 'ind_viv_fin_ult1', 'ind_nomina_ult1', 'ind_nom_pens_ult1', 'ind_recibo_ult1']

KEYS_SIMPLIFY = ['indrel_1mes']

KEYS_FULL = ['age', 'antiguedad', 'canal_entrada_004', 'canal_entrada_007', 'canal_entrada_013', 'canal_entrada_025', 'canal_entrada_K00', 'canal_entrada_KAA', 'canal_entrada_KAB', 'canal_entrada_KAC', 'canal_entrada_KAD', 'canal_entrada_KAE', 'canal_entrada_KAF', 'canal_entrada_KAG', 'canal_entrada_KAH', 'canal_entrada_KAI', 'canal_entrada_KAJ', 'canal_entrada_KAK', 'canal_entrada_KAL', 'canal_entrada_KAM', 'canal_entrada_KAN', 'canal_entrada_KAO', 'canal_entrada_KAP', 'canal_entrada_KAQ', 'canal_entrada_KAR', 'canal_entrada_KAS', 'canal_entrada_KAT', 'canal_entrada_KAU', 'canal_entrada_KAV', 'canal_entrada_KAW', 'canal_entrada_KAY', 'canal_entrada_KAZ', 'canal_entrada_KBB', 'canal_entrada_KBD', 'canal_entrada_KBE', 'canal_entrada_KBF', 'canal_entrada_KBG', 'canal_entrada_KBH', 'canal_entrada_KBJ', 'canal_entrada_KBL', 'canal_entrada_KBM', 'canal_entrada_KBN', 'canal_entrada_KBO', 'canal_entrada_KBP', 'canal_entrada_KBQ', 'canal_entrada_KBR', 'canal_entrada_KBS', 'canal_entrada_KBU', 'canal_entrada_KBV', 'canal_entrada_KBW', 'canal_entrada_KBX', 'canal_entrada_KBY', 'canal_entrada_KBZ', 'canal_entrada_KCA', 'canal_entrada_KCB', 'canal_entrada_KCC', 'canal_entrada_KCD', 'canal_entrada_KCE', 'canal_entrada_KCF', 'canal_entrada_KCG', 'canal_entrada_KCH', 'canal_entrada_KCI', 'canal_entrada_KCJ', 'canal_entrada_KCK', 'canal_entrada_KCL', 'canal_entrada_KCM', 'canal_entrada_KCN', 'canal_entrada_KCO', 'canal_entrada_KCP', 'canal_entrada_KCQ', 'canal_entrada_KCR', 'canal_entrada_KCS', 'canal_entrada_KCT', 'canal_entrada_KCU', 'canal_entrada_KCV', 'canal_entrada_KCX', 'canal_entrada_KDA', 'canal_entrada_KDB', 'canal_entrada_KDC', 'canal_entrada_KDD', 'canal_entrada_KDE', 'canal_entrada_KDF', 'canal_entrada_KDG', 'canal_entrada_KDH', 'canal_entrada_KDI', 'canal_entrada_KDL', 'canal_entrada_KDM', 'canal_entrada_KDN', 'canal_entrada_KDO', 'canal_entrada_KDP', 'canal_entrada_KDQ', 'canal_entrada_KDR', 'canal_entrada_KDS', 'canal_entrada_KDT', 'canal_entrada_KDU', 'canal_entrada_KDV', 'canal_entrada_KDW', 'canal_entrada_KDX', 'canal_entrada_KDY', 'canal_entrada_KDZ', 'canal_entrada_KEA', 'canal_entrada_KEB', 'canal_entrada_KEC', 'canal_entrada_KED', 'canal_entrada_KEE', 'canal_entrada_KEF', 'canal_entrada_KEG', 'canal_entrada_KEH', 'canal_entrada_KEI', 'canal_entrada_KEJ', 'canal_entrada_KEK', 'canal_entrada_KEL', 'canal_entrada_KEM', 'canal_entrada_KEN', 'canal_entrada_KEO', 'canal_entrada_KEQ', 'canal_entrada_KES', 'canal_entrada_KEU', 'canal_entrada_KEV', 'canal_entrada_KEW', 'canal_entrada_KEY', 'canal_entrada_KEZ', 'canal_entrada_KFA', 'canal_entrada_KFB', 'canal_entrada_KFC', 'canal_entrada_KFD', 'canal_entrada_KFE', 'canal_entrada_KFF', 'canal_entrada_KFG', 'canal_entrada_KFH', 'canal_entrada_KFI', 'canal_entrada_KFJ', 'canal_entrada_KFK', 'canal_entrada_KFL', 'canal_entrada_KFM', 'canal_entrada_KFN', 'canal_entrada_KFP', 'canal_entrada_KFR', 'canal_entrada_KFS', 'canal_entrada_KFT', 'canal_entrada_KFU', 'canal_entrada_KFV', 'canal_entrada_KGC', 'canal_entrada_KGN', 'canal_entrada_KGU', 'canal_entrada_KGV', 'canal_entrada_KGW', 'canal_entrada_KGX', 'canal_entrada_KGY', 'canal_entrada_KHA', 'canal_entrada_KHC', 'canal_entrada_KHD', 'canal_entrada_KHE', 'canal_entrada_KHF', 'canal_entrada_KHK', 'canal_entrada_KHL', 'canal_entrada_KHM', 'canal_entrada_KHN', 'canal_entrada_KHO', 'canal_entrada_KHP', 'canal_entrada_KHQ', 'canal_entrada_KHR', 'canal_entrada_KHS', 'canal_entrada_RED', 'canal_entrada_U', 'conyuemp', 'fecha_alta', 'fecha_dato', 'ind_actividad_cliente', 'ind_empleado_A', 'ind_empleado_B', 'ind_empleado_F', 'ind_empleado_N', 'ind_empleado_S', 'ind_empleado_U', 'ind_nuevo', 'indext', 'indfall', 'indrel', 'indrel_1mes', 'indresi', 'nomprov_ALAVA', 'nomprov_ALBACETE', 'nomprov_ALICANTE', 'nomprov_ALMERIA', 'nomprov_ASTURIAS', 'nomprov_AVILA', 'nomprov_BADAJOZ', 'nomprov_BALEARS, ILLES', 'nomprov_BARCELONA', 'nomprov_BIZKAIA', 'nomprov_BURGOS', 'nomprov_CACERES', 'nomprov_CADIZ', 'nomprov_CANTABRIA', 'nomprov_CASTELLON', 'nomprov_CEUTA', 'nomprov_CIUDAD REAL', 'nomprov_CORDOBA', 'nomprov_CORUNA', 'nomprov_CUENCA', 'nomprov_GIPUZKOA', 'nomprov_GIRONA', 'nomprov_GRANADA', 'nomprov_GUADALAJARA', 'nomprov_HUELVA', 'nomprov_HUESCA', 'nomprov_JAEN', 'nomprov_LEON', 'nomprov_LERIDA', 'nomprov_LUGO', 'nomprov_MADRID', 'nomprov_MALAGA', 'nomprov_MELILLA', 'nomprov_MURCIA', 'nomprov_NAVARRA', 'nomprov_OURENSE', 'nomprov_PALENCIA', 'nomprov_PALMAS, LAS', 'nomprov_PONTEVEDRA', 'nomprov_RIOJA, LA', 'nomprov_SALAMANCA', 'nomprov_SANTA CRUZ DE TENERIFE', 'nomprov_SEGOVIA', 'nomprov_SEVILLA', 'nomprov_SORIA', 'nomprov_TARRAGONA', 'nomprov_TERUEL', 'nomprov_TOLEDO', 'nomprov_U', 'nomprov_VALENCIA', 'nomprov_VALLADOLID', 'nomprov_ZAMORA', 'nomprov_ZARAGOZA', 'pais_residencia_AD', 'pais_residencia_AE', 'pais_residencia_AL', 'pais_residencia_AO', 'pais_residencia_AR', 'pais_residencia_AT', 'pais_residencia_AU', 'pais_residencia_BA', 'pais_residencia_BE', 'pais_residencia_BG', 'pais_residencia_BM', 'pais_residencia_BO', 'pais_residencia_BR', 'pais_residencia_BY', 'pais_residencia_BZ', 'pais_residencia_CA', 'pais_residencia_CD', 'pais_residencia_CF', 'pais_residencia_CG', 'pais_residencia_CH', 'pais_residencia_CI', 'pais_residencia_CL', 'pais_residencia_CM', 'pais_residencia_CN', 'pais_residencia_CO', 'pais_residencia_CR', 'pais_residencia_CU', 'pais_residencia_CZ', 'pais_residencia_DE', 'pais_residencia_DJ', 'pais_residencia_DK', 'pais_residencia_DO', 'pais_residencia_DZ', 'pais_residencia_EC', 'pais_residencia_EE', 'pais_residencia_EG', 'pais_residencia_ES', 'pais_residencia_ET', 'pais_residencia_FI', 'pais_residencia_FR', 'pais_residencia_GA', 'pais_residencia_GB', 'pais_residencia_GE', 'pais_residencia_GH', 'pais_residencia_GI', 'pais_residencia_GM', 'pais_residencia_GN', 'pais_residencia_GQ', 'pais_residencia_GR', 'pais_residencia_GT', 'pais_residencia_GW', 'pais_residencia_HK', 'pais_residencia_HN', 'pais_residencia_HR', 'pais_residencia_HU', 'pais_residencia_IE', 'pais_residencia_IL', 'pais_residencia_IN', 'pais_residencia_IS', 'pais_residencia_IT', 'pais_residencia_JM', 'pais_residencia_JP', 'pais_residencia_KE', 'pais_residencia_KH', 'pais_residencia_KR', 'pais_residencia_KW', 'pais_residencia_KZ', 'pais_residencia_LB', 'pais_residencia_LT', 'pais_residencia_LU', 'pais_residencia_LV', 'pais_residencia_LY', 'pais_residencia_MA', 'pais_residencia_MD', 'pais_residencia_MK', 'pais_residencia_ML', 'pais_residencia_MM', 'pais_residencia_MR', 'pais_residencia_MT', 'pais_residencia_MX', 'pais_residencia_MZ', 'pais_residencia_NG', 'pais_residencia_NI', 'pais_residencia_NL', 'pais_residencia_NO', 'pais_residencia_NZ', 'pais_residencia_OM', 'pais_residencia_PA', 'pais_residencia_PE', 'pais_residencia_PH', 'pais_residencia_PK', 'pais_residencia_PL', 'pais_residencia_PR', 'pais_residencia_PT', 'pais_residencia_PY', 'pais_residencia_QA', 'pais_residencia_RO', 'pais_residencia_RS', 'pais_residencia_RU', 'pais_residencia_SA', 'pais_residencia_SE', 'pais_residencia_SG', 'pais_residencia_SK', 'pais_residencia_SL', 'pais_residencia_SN', 'pais_residencia_SV', 'pais_residencia_TG', 'pais_residencia_TH', 'pais_residencia_TN', 'pais_residencia_TR', 'pais_residencia_TW', 'pais_residencia_U', 'pais_residencia_UA', 'pais_residencia_US', 'pais_residencia_UY', 'pais_residencia_VE', 'pais_residencia_VN', 'pais_residencia_ZA', 'pais_residencia_ZW', 'renta', 'segmento_01 - TOP', 'segmento_02 - PARTICULARES', 'segmento_03 - UNIVERSITARIO', 'segmento_U', 'sexo', 'tiprel_1mes_A', 'tiprel_1mes_I', 'tiprel_1mes_N', 'tiprel_1mes_P', 'tiprel_1mes_R', 'tiprel_1mes_U', 'ult_fec_cli_1t']


def processChunk(chunk):
    assert isinstance(chunk, DataFrame)

    #Remove useless keys
    try:
        chunk.drop(KEYS_DROPPED, axis=1, inplace=True)
    except:
        print "ERR: Failed to drop keys!"

    #Remove NAs
    #chunk.replace(' NA', np.nan, inplace=True)
    #chunk.replace('NA', np.nan, inplace=True)


    #Replace the binary stringss with actual binary values
    chunk[KEYS_BINARY] = chunk[KEYS_BINARY].fillna(False)
    chunk[KEYS_BINARY] = chunk[KEYS_BINARY].replace(['V', 'N', '0', 0], False)
    chunk[KEYS_BINARY] = chunk[KEYS_BINARY].replace(['H', 'S', '1', 1], True)
    chunk[KEYS_BINARY] = chunk[KEYS_BINARY].astype(np.bool_)

    #Special cases
    chunk['indrel'] = chunk['indrel'].replace('99', False)
    chunk['indrel'] = chunk['indrel'].replace(' 1', True)
    chunk['indrel'].astype(np.bool_)
    chunk['nomprov'] = chunk['nomprov'].replace('CORU\xc3\x91A, A', 'CORUNA')


    #Make indrel_1mes more categorical
    chunk[KEYS_SIMPLIFY] = chunk[KEYS_SIMPLIFY].fillna(-1)
    chunk[KEYS_SIMPLIFY] = chunk[KEYS_SIMPLIFY].replace(['1.0', '1'], 1)
    chunk[KEYS_SIMPLIFY] = chunk[KEYS_SIMPLIFY].replace(['2.0', '2'], 2)
    chunk[KEYS_SIMPLIFY] = chunk[KEYS_SIMPLIFY].replace(['3.0', '3'], 3)
    chunk[KEYS_SIMPLIFY] = chunk[KEYS_SIMPLIFY].replace(['4.0', '4'], 4)
    chunk[KEYS_SIMPLIFY] = chunk[KEYS_SIMPLIFY].replace('P', 5)
    chunk['indrel'].astype(np.int8)

    #Remove NAN for symbolics
    chunk[KEYS_SYMBOLIC] = chunk[KEYS_SYMBOLIC].replace(['', 'nan', 'NA', ' NA'], 'U')
    chunk[KEYS_SYMBOLIC] = chunk[KEYS_SYMBOLIC].fillna('U')

    # Parse Date Fields as Ints
    for col in KEYS_DATES:
        chunk[col] = PD.to_datetime(chunk[col])
        chunk[col] = chunk[col].map(lambda x: x.month)
        chunk[col].fillna(0, inplace=True)
        chunk[col].astype(np.int8)

    #KEYS NORMED FILL NAN
    for col in KEYS_NORMED:
        chunk[col] = PD.to_numeric(chunk[col], errors='coerce')
    chunk['renta'].fillna(134254, inplace=True)
    chunk['antiguedad'].fillna(80, inplace=True)
    chunk['age'].fillna(40, inplace=True)


    hotcodes = PD.get_dummies(chunk[KEYS_SYMBOLIC], prefix=KEYS_SYMBOLIC, columns=KEYS_SYMBOLIC)
    hotcodes = hotcodes.astype(np.bool_)

    chunk.drop(KEYS_SYMBOLIC, axis=1, inplace=True)
    chunk = PD.concat([chunk, hotcodes], axis=1)

    return chunk


def normalize(incoming):
    # Normalize the continuous fields, dates will be normed
    incoming[KEYS_NORMED] = incoming[KEYS_NORMED].apply(lambda x: (x - x.mean()) / (x.max() - x.min()))
    return incoming

def forceshape(incoming):
    # Force the dataframe to be a certain size so its consistent across data-inputs
    for item in KEYS_FULL:
        if item not in incoming:
            incoming[item] = 0
    return incoming


def multishape(data, features):
    assert isinstance(data, DataFrame)
    #Remove extraneous columns that dont exist in feature space
    droppingcols = []
    #Create a list of columns that should be dropped
    for col in data.columns:
        if col not in features:
            droppingcols.append(col)

    #Drop the columns all at once
    if len(droppingcols) > 0:
        print "Dropping columns not present in training: ", len(droppingcols)
        data.drop(droppingcols, axis=1, inplace=True)

    #add columns that dont already exist but should from features
    added = 0
    for f in features:
        if f not in data.columns:
            added = added + 1
            data[f] = 0
    if ( added > 0):
        print added, " columns were added that were not part of the training"
    return data

def postprocess(data):
    data.fillna(0, inplace=True)
    return data

def splitXY(incoming):
    """
       @type incoming: DataFrame
       @param incoming:
       @return: data, classLabel
       @rtype: DataFrame
    """
    x = incoming

    if KEYS_PRODUCTS[0] in incoming.columns:
        ytargets = x[KEYS_PRODUCTS]
        ytargets = ytargets.fillna(False)
        ytargets.replace(['0', '1'], [0, 1], inplace=True)
        ytargets = ytargets.astype(np.bool_)
        x.drop(KEYS_PRODUCTS, axis=1, inplace=True)
    else:
        ytargets = DataFrame()

    return x, ytargets