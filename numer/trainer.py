import pandas as pd
from keras.callbacks import EarlyStopping
from keras.layers import Dense
from keras.models import Sequential
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier

'''
Chris Aprea
Machine Learning for Numer
'''

'''
CONFIGURATION
'''
FILE_TRAIN = '/home/chris/datasets/numer_datasets/numerai_training_data.csv'
EPOCHS = 3
DEV = True

# Read the data
print("Loading training data ", FILE_TRAIN)
# Load the data from the CSV files
training_data = pd.read_csv(FILE_TRAIN, header=0)

print("Preparing training data")
rawY = training_data['target']
rawX = training_data.drop('target', axis=1)

print("Dimensionality reduction")

# Convert to numpy arrays for feeding into ANN
if DEV:
    nX = rawX[1:2200].values
    nY = rawY[1:2200].values

    # Do dimensionality reduction


else:
    nX = rawX.values
    nY = rawY.values

print("Splitting data into sets")
x_train, x_val, y_train, y_val = train_test_split(nX, nY, test_size=0.20, random_state=55)


def trainNN():
    print("Making Keras ANN")
    width = rawX.shape[1]
    ann_model = Sequential()
    ann_model.add(Dense(width, input_dim=width, init='uniform', activation='relu'))
    ann_model.add(Dense(width, init='uniform', activation='relu'))
    ann_model.add(Dense(1, init='uniform', activation='softmax'))  # sigmoid

    # Compile model
    print ("Compiling Model")
    ann_model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    early_stopping = EarlyStopping(monitor='val_loss', patience=500)

    print("Training the ANN")
    ann_model.fit(x_train, y_train, nb_epoch=EPOCHS, batch_size=100, validation_data=(x_val, y_val), verbose=1,
                  callbacks=[early_stopping])

    print("Evaluating the models")

    print("Saving the ANN")
    ann_model.save('numer.ai.ann')


def trainSK():
    clfnames = [
        "Nearest Neighbors",
        "Linear SVM",
        "RBF SVM",
        "Gaussian Process",
        "Decision Tree",
        "Random Forest",
        "Neural Net",
        "AdaBoost",
        "Naive Bayes",
        "QDA"
    ]

    print("Creating models")
    classifiers = [
        KNeighborsClassifier(),
        SVC(kernel="linear"),
        SVC(kernel="rbf"),
        GaussianProcessClassifier(),
        DecisionTreeClassifier(),
        RandomForestClassifier(),
        MLPClassifier(),
        AdaBoostClassifier(),
        GaussianNB(),
        QuadraticDiscriminantAnalysis()
    ]

    print ("Running classifiers")
    # iterate over classifiers
    results = {}
    for name, clf in zip(clfnames, classifiers):
        # scores = cross_val_score(clf, X_train, y_train, cv=5)
        print ("Classifier: ", name)
        scores = cross_val_score(clf, x_train, y_train, verbose=0, cv=5, n_jobs=-1)
        results[name] = scores

    for name, scores in results.items():
        print("%20s | Accuracy: %0.2f%% (+/- %0.2f%%)" % (name, 100 * scores.mean(), 100 * scores.std() * 2))

    '''
    Use SEABORN TO PLOT GRAPHS
    '''
    # plt.show()


if __name__ == "__main__":
    # trainNN()
    trainSK()
    print "Done"
