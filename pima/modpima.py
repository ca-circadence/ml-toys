'''
Christopher Aprea
Created August 2016
'''

#/home/ca/data/pima-indians-diabetes.data
import time

datafile = "pima-indians-diabetes.data"
numLayers = 5
layerMaxSize = 200

numFeatures = 8



# Create first network with Keras
from keras.models import Sequential
from keras.layers import Dense

import numpy
# fix random seed for reproducibility
seed = 5
numpy.random.seed(seed)
# load pima indians dataset
dataset = numpy.loadtxt(datafile, delimiter=",")
# split into input (X) and output (Y) variables
X = dataset[:,0:numFeatures]
Y = dataset[:,numFeatures]

print "Data Loaded"

# create model
model = Sequential()
model.add(Dense(numFeatures * 1.5, input_dim=numFeatures, init='uniform', activation='relu'))

for i in range(0, numLayers):
    n = numpy.random.randint(numFeatures-1, layerMaxSize)
    model.add(Dense(n, init='uniform', activation='relu'))


model.add(Dense(1, init='uniform', activation='sigmoid'))

# Compile model
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])


print "Model compiled"
print "Training..."
time.sleep(0.1)
# Fit the model
model.fit(X, Y, nb_epoch=2000, batch_size=20, verbose=1, callbacks=[], validation_split=0.10)
time.sleep(0.1)
# evaluate the model
scores = model.evaluate(X, Y)
print("\n %s: %.2f %s: %.2f" % (model.metrics_names[1], scores[1], model.metrics_names[0], scores[0]))

model.save('model')