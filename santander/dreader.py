


import pandas as PD
from pandas import DataFrame
import sqlalchemy
import dataparser

DATA_PATH = '/home/chris/datasets/santander/'
TRAIN1 = DATA_PATH + 'train.sample.csv'
TRAIN2 = DATA_PATH + 'train_ver2.csv'
TEST_FILE = DATA_PATH + 'test_ver2.csv'
TRAIN_FILE = TRAIN2


engine = sqlalchemy.create_engine('postgresql://postgres:4thRock@10.20.0.143:5432/chris')

BATCHSIZE = 5000

dftrainraw = PD.read_csv(TRAIN_FILE, chunksize=BATCHSIZE, dtype=str, index_col='ncodpers')

c = 0

#Iterate over each piece of the data set
# LOAD THE TRAINING DATA
trainx = DataFrame()
trainy = DataFrame()


for part in dftrainraw:
    c = c + 1
    #x = dataparser.sanitize(part)
    print  "Chunk ", c, " ", part.shape
    d, t = dataparser.splitXY(part)
    d = dataparser.processChunk(d)

    print "Putting in SQL..."
    d.to_sql('sant_train_d', engine, if_exists='append', index=True, index_label='ncodpers', chunksize=BATCHSIZE)
    t.to_sql('sant_train_t', engine, if_exists='append', index=True, index_label='ncodpers', chunksize=BATCHSIZE )

print "===============\n"


#keys= dataparser.KEYS_TEMPLATE
#sf = DataFrame(columns=keys)
#sf.ix[1] = 1
#sf.fillna(False, inplace=True)
#sf.to_sql('sant_train_d', engine, if_exists='replace', index=True, index_label='ncodpers')
