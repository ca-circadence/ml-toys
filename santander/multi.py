
'''
Hello World

Project: Santander product prediction
'''

import pandas as PD
from pandas import DataFrame
import dataparser
import time
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
import sklearn.ensemble as ENSEMBLE
DATA_PATH = '/home/chris/datasets/santander/'
TRAIN1 = DATA_PATH + 'train.sample.csv'
TRAIN2 = DATA_PATH + 'train_ver2.csv'
TEST_FILE = DATA_PATH + 'test_ver2.csv'
TRAIN_FILE = TRAIN1

NUM_NEURONS = 125
NUM_LAYERS = 2
NUM_EPOCHS = 5
NUM_FORESTS = 10
RATIO_VALIDATION = 0.10
RATIO_DROP = 0.40

BATCHSIZE = 15000

dftrainraw = PD.read_csv(TRAIN_FILE, chunksize=BATCHSIZE, dtype=str, index_col='ncodpers')


#For keeping track of how long each process takes
def tic():
    t = time.time()
    return t

def toc(t):
    delta = time.time() - t
    return delta


# LOAD THE TRAINING DATA
#Iterate over each piece of the data set
c = 0
trainx = DataFrame()
trainy = DataFrame()
for part in dftrainraw:
    c = c + 1
    print  "Chunk ", c, " ", part.shape
    s, t = dataparser.splitXY(part)
    s = dataparser.processChunk(s)
    assert (s.isnull().any().any() == False)
    trainx = PD.concat([trainx, s], ignore_index=True)
    trainy = PD.concat([trainy, t], ignore_index=True)

print "===============\n"
trained_features = dataparser.KEYS_TEMPLATE
print "Features Used:\n ", len(trained_features)
trainx = dataparser.postprocess(trainx)
trainx = dataparser.multishape(trainx, trained_features)

print "Normalizing"
trainx = dataparser.normalize(trainx)
class_names = trainy.columns

#==========================
#Training phase
#===========================
#Create the model
def mmodel(insize, outsize):
    model = Sequential()

    model.add(Dense(insize, input_dim=insize, init='uniform', activation='linear'))
    #Add hidden layers layers
    for i in xrange(1, NUM_LAYERS):
        model.add(Dense(NUM_NEURONS, init='uniform', activation = 'relu'))
        model.add(Dropout(RATIO_DROP))
    model.add(Dense(outsize, init='uniform', activation='sigmoid'))
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

    return model

#Create a NN for predicting each product line
print "Starting training sessions"
s = tic()
ann = mmodel(trainx.shape[1], trainy.shape[1])
ann.fit(trainx.values, trainy.values, batch_size=10000, verbose=1, nb_epoch=NUM_EPOCHS, validation_split=RATIO_VALIDATION)
print "Training ANN took ", toc(s), " seconds"
s = tic()
forest = ENSEMBLE.RandomForestClassifier(NUM_FORESTS, "gini")
forest.fit(trainx.values, trainy.values)
print "Training on forest took ", toc(s), " seconds"
#===========================
#Prediction phase
#=============================
print "Reading in test data", time.time()
df = PD.read_csv(TEST_FILE, index_col='ncodpers', dtype=str)
testx, dummy = dataparser.splitXY(df)
testx = dataparser.processChunk(testx)
testx = dataparser.postprocess(testx)
testx = dataparser.multishape(testx, trained_features)
testx = dataparser.normalize(testx)


print "Predicting", testx.shape, " ", time.time()
s = tic()
pred_ann = ann.predict(testx.values, verbose=0)
pred_forest = forest.predict(testx.values)
print "Predicting on took ", toc(s)

print "Preparing output"
combined_predictions = (pred_ann + pred_forest)
combined_predictions = combined_predictions / 2.0

preds = DataFrame(combined_predictions, index=testx.index)
binrep = (preds > 0.5).astype(bool)


def getlbls(dfrow):
    matches = []
    c = 0
    for value in dfrow:
        if (value == True):
            matches.append(class_names[c])
        c = c + 1
    line = ' '.join(matches)
    return line

print "Converting to string labels"
strpre = list()
for idx, row in binrep.iterrows():
    strpre.append(getlbls(row.values))

testx['added_products'] = strpre

print "Saving"
#Output predictions
testx.to_csv('predictions.csv', index_label='ncodpers', columns=['added_products'] )


print "debug"
print "Done with results"


print "Ready/Done"

