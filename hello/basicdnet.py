"""
@author Christopher Aprea
@company Circadence
@date August 2016
"""

import os
import numpy as np
import keras
import os

from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Convolution2D, MaxPooling2D
from keras.utils import np_utils
import time as TIME


batch_size = 128
nb_classes = 10
nb_epoch = 12

# input image dimensions
img_rows, img_cols = 28, 28
# number of convolutional filters to use
nb_filters = 32
# size of pooling area for max pooling
nb_pool = 2
# convolution kernel size
nb_conv = 3

def getData(dataFile):
    print 'trying to get data'
    (X_train, y_train), (X_test, y_test) = mnist.load_data()
    return (X_train, y_train), (X_test, y_test)

def convertLabel(data):
    mdata = np_utils.to_categorical(data, nb_classes) # for use with categorical crossentropy
    return mdata

def preprocess(data):
    print 'Prior shape ', data.shape
    mdata = data.reshape(data.shape[0], 1, img_rows, img_cols)
    mdata = mdata.astype('float32')
    mdata /= 255
    print 'Post shape ', mdata.shape
    return mdata


def createModelFF():
    model = Sequential()
    model.add(Flatten(input_shape=(1, img_rows, img_cols)))
    model.add(Dense(128))
    model.add(Activation('relu'))
    model.add(Dense(128))
    model.add(Dense(nb_classes))
    model.add(Activation('softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    print(model.summary())
    return model

def train(model, x, y, valx, valy):
    TIME.sleep(0.1)
    model.fit(x, y, batch_size=5000, nb_epoch=nb_epoch, verbose=1, validation_data=(valx, valy))
    TIME.sleep(0.1)

    score = model.evaluate(valx, valy, verbose=0)
    print('Test score:', score[0])
    print('Test accuracy:', score[1])

def createModelCNN():
    model = Sequential()

    model.add(Convolution2D(nb_filters, nb_conv, nb_conv, border_mode='valid', input_shape=(1, img_rows, img_cols)))
    model.add(Activation('relu'))
    model.add(Convolution2D(nb_filters, nb_conv, nb_conv))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(nb_pool, nb_pool)))
    model.add(Dropout(0.25))

    model.add(Flatten())
    model.add(Dense(128))
    model.add(Activation('relu'))
    model.add(Dropout(0.5))
    model.add(Dense(nb_classes))
    model.add(Activation('softmax'))

    model.compile(loss='categorical_crossentropy', optimizer='adadelta', metrics=['accuracy'])
    return model


def main():
    print "Starting"
    (x, y), (xtest, ytest)  = getData('datafile')
    print "Data finished"
    y = convertLabel(y)
    ytest = convertLabel(ytest)
    print "Processing..."
    x = preprocess(x)
    xtest = preprocess(xtest)
    net = createModelFF()

    train(net, x, y, xtest, ytest)
    print 'done'


if (__name__ == "__main__"):
    main()