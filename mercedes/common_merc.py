
import pandas as pd
from pandas import DataFrame

FIELDS_SYMBOLIC = ['X0', 'X1', 'X2', 'X3', 'X4', 'X5', 'X6', 'X8']


def transform(data):
    """
    :param data:
    :type data: DataFrame
    :return:
    """
    # X0-X8 are symbolic
    # X0.. X8

    result = None
    symb = pd.get_dummies(data, columns=FIELDS_SYMBOLIC)
    return symb

def normalize(data):
    """
    Applies normalization function to data such that data is
    on the range of [0,1]
    :param data:
    :type data: DataFrame
    :return:
    """
    norm = (data - data.mean()) / (data.max() - data.min())
    return norm

def denorm(data, lower, middle, upper):
    """
    Reverses
    :param data:
    :return:
    """
    reg = (data * (upper - lower)) + middle
    return reg

def selective_omit(data, cols):
    """
    Selectively removes columns from data that are not present in cols
    :param data:
    :param cols:
    :type data: DataFrame
    :type cols: list
    :return:
    """
    #Remove columns from data that are not present in cols (list)
    for c in data.columns:
        if c not in cols:
            print "dropping ", c
            data.drop(c, axis=1, inplace=True)

    return data
