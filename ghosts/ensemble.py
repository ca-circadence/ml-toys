import matplotlib.pyplot as plt

import pandas
import seaborn as sns
from sklearn import preprocessing as SKPRE
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier

TRAIN_FILE = 'train.csv'
dtypes = {'bone_length': 'float32', 'rotting_flesh': 'float32', 'hair_length': 'float32', 'has_soul': 'float16',
          'color': 'str', 'type': 'str'}
raw = pandas.read_csv(TRAIN_FILE, dtype=dtypes, index_col='id', verbose=0)

y = raw['type']
x = raw.drop('type', axis=1)

encoder = SKPRE.LabelEncoder()
x['color'] = encoder.fit_transform((x['color']))

# to convert back

X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.25, random_state=0)

names = ["Nearest Neighbors", "Linear SVM", "RBF SVM", "Gaussian Process",
         "Decision Tree", "Random Forest", "Neural Net", "AdaBoost",
         "Naive Bayes", "QDA"
         ]

classifiers = [
    KNeighborsClassifier(),
    SVC(kernel="linear"),
    SVC(kernel="rbf"),
    GaussianProcessClassifier(),
    DecisionTreeClassifier(),
    RandomForestClassifier(),
    MLPClassifier(),
    AdaBoostClassifier(),
    GaussianNB(),
    QuadraticDiscriminantAnalysis()
]

# iterate over classifiers
results = {}
for name, clf in zip(names, classifiers):
    # scores = cross_val_score(clf, X_train, y_train, cv=5)
    scores = cross_val_score(clf, X_train, y_train, cv=5, n_jobs=1)
    results[name] = scores

for name, scores in results.items():
    print("%20s | Accuracy: %0.2f%% (+/- %0.2f%%)" % (name, 100 * scores.mean(), 100 * scores.std() * 2))

'''
Use SEABORN TO PLOT GRAPHS
'''


sns.set_color_codes("muted")
sns.barplot(x='Accuracy', y='Classifier', data=scores['Accuracy'], color="b")

plt.xlabel('Accuracy %')
plt.title('Classifier Accuracy')
plt.show()

sns.set_color_codes("muted")
sns.barplot(x='Log Loss', y='Classifier', data=scores['log'], color="g")

plt.xlabel('Log Loss')
plt.title('Classifier Log Loss')
plt.show()
