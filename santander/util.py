


import time

DATA_PATH = '/home/chris/datasets/santander/'
TRAIN1 = DATA_PATH + 'train.sample.csv'
TRAIN2 = DATA_PATH + 'train_ver2.csv'
TEST_FILE = DATA_PATH + 'test_ver2.csv'
TRAIN_FILE = TRAIN1

NUM_NEURONS = 5
NUM_LAYERS = 1
NUM_EPOCHS = 1
NUM_FORESTS = 1
RATIO_VALIDATION = 0.10
RATIO_DROP = 0.40

BATCHSIZE = 15000

STOREFILE = 'model.pickle'

#For keeping track of how long each process takes
def tic():
    t = time.time()
    return t

def toc(t):
    delta = time.time() - t
    return delta