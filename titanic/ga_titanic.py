
"""
@author Christopher Aprea
@date September 2016
http://log.or.cz/?p=386
"""
import csv
import numpy
import pandas as PD
from pandas import DataFrame
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.models import load_model
import random as RANDOM
from keras.optimizers import SGD
import time as TIME
import numbers

#test comment

KEYS_ENCODED = ['Embarked', 'Name', 'Cabin', 'Pclass']

KEYS_IGNORED = ['Fare','PassengerId','Ticket','Parch','SibSp']
KEYS_NORMED = ['Age']
SESS = 30000
LAYERS = 5
LAYER_SIZE = 25
SAMPLE_SIZE = 0.50
FORGETNESS = 0.5
MUTATION_CHANCE = 0.10

def getData(dataFile):
    #Read the input as a Pandas DataFrame
    data = PD.read_csv(dataFile)
    return data


def cleanData(data):
    """
    @type data: DataFrame
    @param data:
    @return: features, classLabel
    @rtype: DataFrame
    """
    assert isinstance(data, DataFrame)
    #Clean up the genders, convert from str to [0,1]
    data.replace('male', 1, inplace=True)
    data.replace('female', 0, inplace=True)


    #Get the survival label
    survived = None
    try:
        survived = data.pop('Survived')
    except:
        print "Labeled field not found"


    #RegExp matches for Miss.|Mrs.|Don.|etc..
    expr = '(.+,\s)([\s,A-Z,a-z]+)(\..*)'
    data['Name'].replace(expr, value='\\2', regex=True, inplace=True)
    expr = '(Dona|Lady|Mlle|Mme|the Countess)+'
    data['Name'].replace(expr, value='Lady', regex=True, inplace=True)
    expr = '(Capt|Col|Don|Major|Master|Sir|Jonkheer)+'
    data['Name'].replace(expr, value='Sir', regex=True, inplace=True)
    expr= '([A-Z])+\d*(.*)+'
    data['Cabin'].replace(expr, value='\\1', regex=True, inplace=True)



    #Add the 'family size' feature
    fam = DataFrame(data['SibSp'] + data['Parch'], columns=['family'])
    fam.fillna(0, inplace=True)
    data = PD.concat([data, fam], axis=1)


    # Remove the passengerid, and name columns because they are not useful and not easily encoded
    data.drop(KEYS_IGNORED, axis=1, inplace=True)

    # One-Hot Encode the Embarked location
    print "Hot-Encoding and Filling Modes..."
    print data[KEYS_ENCODED].mode()
    data[KEYS_ENCODED].fillna(data[KEYS_ENCODED].mode(), inplace=True)
    hotcodes = PD.get_dummies(data[KEYS_ENCODED])
    data = PD.concat([data, hotcodes], axis=1)
    data.drop(KEYS_ENCODED, axis=1, inplace=True)



    # replace NaN with average value
    avgs = data.mean()
    data = data[avgs.keys()].fillna(avgs, inplace=True)



    # Assuming same lines from your example
    #data[KEYS_NORMED] = data[KEYS_NORMED].apply(lambda x: (x - x.mean()) / (x.max() - x.min()))


    #final data

    print data.keys()
    return data, survived



def createModelFF(dimension, params):
    start = 'uniform'

    model = Sequential()

    s = int(params['layersize'])
    n = int(params['layers'])

    model.add(Dense(s, input_dim=dimension, init=start, activation='linear'))

    for n in xrange(1, n):
        model.add(Dense(s, init=start, activation=params['activation']))
        model.add(Dropout(params['forgetness']))


    model.add(Dense(1, init=start, activation='sigmoid'))
    #model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    model.compile(loss='binary_crossentropy', optimizer='rmsprop', metrics=['accuracy'])
    #print(model.summary())
    return model


def createModel(dfX, dfY, hyperparams):
    """
    :type trainx: DataFrame
    :type trainy: DataFrame

    :return: model
    :rtype: Sequential, int
    """
    print "Creating model...", hyperparams

    # Get a random sample of the data, save the holdout as validation
    s = hyperparams['samplesize']
    if (s >= 0.85):
        s = 0.85

    tX = dfX.sample(frac=0.85, random_state=RANDOM.randint(1,90))
    vX = dfX.drop(tX.index)  # validiation X is ~trainX

    tY = dfY[tX.index]  # train Y is same indexs as trainX
    vY = dfY.drop(tX.index)  # validation Y is ~trainX

    model = createModelFF(dfX.shape[1], hyperparams)
    s = train(model, tX.values, tY.values, vX.values, vY.values)

    return model, s


def train(model, trainx, trainy, valx, valy):
    trainX = trainx
    trainY = trainy

    TIME.sleep(0.1)
    model.fit(trainX, trainY, batch_size=800, nb_epoch=SESS, verbose=0, validation_data=(valx, valy))
    TIME.sleep(0.1)

    score = model.evaluate(valx, valy, verbose=0)

    print('Test score:', score[0])
    print('Test accuracy:', score[1])
    return score[1]

def findBest():
    '''
    Do until fitness (accuracy > 0.8)
        Train N models with random params,

        Take top F models based on accuracy

        Train N models based on (F) with some slight variation
    '''

    '''
    Load the Data, which will be re-used often
    '''
    training = getData('train.csv')
    X, Y = cleanData(training)


    NUMM = 10
    childparm = dict(layers=2, layersize=20, activation='linear', forgetness=0.1, samplesize=0.5)

    bestConfig = None
    bestAcc = 0
    F = 0
    while (bestAcc < 0.999999):
        try:

            F = F + 1
            records = DataFrame(columns=['model', 'params', 'fitness'])
            for n in xrange(1, NUMM):
                p = mutation(childparm)
                created, acc = createModel(X, Y, p)
                records = records.append( DataFrame({'model':created, 'params':[p], 'fitness':acc }), ignore_index=True )

            #Take top half and breed them
            records.sort_values('fitness', ascending=False, inplace=True, na_position='last')

            father = records.iloc[1]
            if ( bestConfig is None):
                mother = records.iloc[2]
            else:
                mother = bestConfig

            childparm = crossover(father['params'], mother['params'])

            if ( father['fitness'] > bestAcc):
                father['model'].save('titanic.nn')
                bestAcc = father['fitness']
                bestConfig = father

            print "Best Acc: ", father['fitness'], " / ", bestAcc
            print "End epoch ", F
        except Exception as err:
            print 'ERROR:', err


def crossover(left, right):
    """
    Cross-over method for Genetic Algorithm to combine two sets of hyper-parameters
    :type left: dict
    :type right: dict
    :param left:
    :param right:
    :return:
    """
    #Take left parameter and combine with right parameter on a chance basis
    child = dict()
    for key in left:
        if RANDOM.random() > 0.5:
            child[key] = left[key]
        else:
            child[key] = right[key]
    return child

def mutation(gene):
    """
    Mutation method for Genetic Algorithm to slightl alter a set of hyper-parameters
    :param gene:
    :return:
    :rtype: dict
    """
    for key in gene:
        if RANDOM.random() <= MUTATION_CHANCE:
            v = gene[key]

            #Only change numeric things
            if isinstance(v, numbers.Number):
                v = RANDOM.gauss(v,v/3.0)
                if ( v < 0):
                    v = 0
                gene[key] = v

    return gene



def predictions():
    """
            Batch Prediction
    """
    model = load_model('titanic.nn')

    print "Program ended"
    test = getData('test.csv')
    pids = test['PassengerId'].values

    dfX, dfY = cleanData(test)

    y = model.predict_classes(dfX.values, verbose=1)
    assert isinstance(y, numpy.ndarray)

    pids = pids.reshape(y.shape)

    output = numpy.hstack( (pids, y) )

    numpy.savetxt('output.csv', output, fmt='%i', header='PassengerId,Survived', delimiter=',', comments='')

    print "Done with ", y.size, " results"

def main():
    print "Program started"

    #findBest()
    predictions()


if "__main__" == __name__:
    main()

