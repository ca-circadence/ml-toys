import time

import numpy
import pandas
import sklearn
import sklearn.ensemble as ENSEMBLE
import sklearn.svm as SVM
from keras.callbacks import EarlyStopping
from keras.layers import Dense, Dropout
from keras.models import Sequential
from keras.utils.np_utils import to_categorical
from pandas import DataFrame
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.naive_bayes import GaussianNB


print "Started learning", time.time()


TRAIN_FILE = 'train.csv'
TEST_FILE = 'test.csv'
OUTPUT = 'submit.csv'
OPT_CHUNKSIZE = 900
NUM_LAYERS = 0
NUM_NEURONS = 20
NUM_SESS = 3000
RATIO_DROP = 0.1
OPT_NN_BATCHSIZE = 100
OPT_ACTIVATION = 'uniform'
RATIO_VALIDATION = 0.50


dtypes = {'bone_length':'float16', 'rotting_flesh':'float16', 'hair_length':'float16', 'has_soul':'float16', 'color':'str', 'type':'str'}
valid_colors = ['clear','green', 'black', 'white', 'blue', 'blood']
valid_labels = ['Ghost','Ghoul', 'Goblin' ]

KEYS_FEATURES = ['bone_length', 'rotting_flesh', 'hair_length', 'has_soul'].append(valid_colors)
KEYS_ENCODE = ['color', 'type']

time_start = time.time()

def createFANN(insize, outsize):
    '''
    Creates Feed-Forward Artificial Neural Network with specified dimensions
    :type insize: int
    :type outsize: int
    :rtype: Sequential
    '''
    model = Sequential()
    model.add(Dense(insize, input_dim=insize, init=OPT_ACTIVATION, activation='linear'))
    #Add hidden layers layers
    for i in xrange(1, NUM_LAYERS):
        model.add(Dense(NUM_NEURONS, init=OPT_ACTIVATION, activation='relu'))
        model.add(Dropout(RATIO_DROP))
    model.add(Dense(outsize, init=OPT_ACTIVATION, activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='rmsprop', metrics=['accuracy'])
    return model



def tic():
    delta = time.time() - time_start
    print "T+", delta


def splitXY(data):
    '''
    Splits the Data between features in left DF and class labels in right DF
    :type data: DataFrame
    :rtype: DataFrame, DataFrame
    '''
    y = data['type']
    x = data.drop('type', axis=1)
    return x, y


def hotencode(data, cols):
    '''
    Turns symbolic into one-hot-encoded array
    :type data: DataFrame
    :rtype: DataFrame
    '''
    #Get the OHE for the specified columns
    coded = pandas.get_dummies(data, prefix='', columns=[cols], prefix_sep='')
    return coded

def int2str(val):
    return valid_labels[val]

def int2hot(val):
    arr = [0, 0, 0]
    arr[val] = 1
    return numpy.ndarray(arr)

def hot2string(encoded, colnames):
    '''
    Converts hod-coded class to a string
    :type encoded: DataFrame
    :rtype: str
    '''
    highProb = -1
    lastindex = len(encoded)
    bestLabel = 'Unknown'
    for c in xrange(0, lastindex):
        if encoded[c] > highProb:
            bestLabel = colnames[c]
            highProb = encoded[c]

    #print 'E: {:3.2f}  {:3.2f}  {:3.2f}   [{}]'.format(encoded[0], encoded[1], encoded[2], bestLabel)
    return bestLabel

def phaseLearn():
    '''
    ==================
    LEARNING PROCESS
    ==================
    '''
    '''
    CREATE MODELS
    '''
    pdreader = pandas.read_csv(TRAIN_FILE, dtype=dtypes, index_col='id', verbose=0)
    model_ann = createFANN(4, 3)
    model_forest = ENSEMBLE.RandomForestClassifier(n_estimators=1000, max_depth=20, random_state=1123, n_jobs=-1)
    model_one = sklearn.multiclass.OneVsRestClassifier(SVM.SVC(decision_function_shape='ovo', cache_size=1000, tol=1e-8))
    model_nb = GaussianNB()
    model_logistic = LogisticRegression(penalty='l2',C=1000000, multi_class='multinomial', n_jobs=-1, verbose=1, tol=1e-7, random_state=123, solver='newton-cg', max_iter=10000)
    model_sgd = SGDClassifier(random_state=123, n_jobs=-1)
    tic()

    '''
    xxxxxxxxx
    '''

    x_train, y_train = splitXY(pdreader)
    x_train.drop('color', axis=1, inplace=True)

    linY = y_train.replace(valid_labels, [0,1,2])

    print "Debug: ", x_train.shape, " / ", linY.shape

    print "Training SGD"
    model_sgd.fit(x_train.values, linY.values)

    print "Training NB"
    model_nb.fit(x_train.values, linY.values)

    print "Training Logistic R"
    model_logistic.fit(x_train.values, linY.values)

    print "Training Forest"
    model_forest.fit(x_train.values, linY.values)

    print "Training SVM"
    model_one.fit(x_train.astype(float).values, linY.astype(float).values)

    print "Training ANN"
    early_stopping = EarlyStopping(monitor='val_acc', mode='max', patience=350, verbose=0)
    model_ann.fit(x_train.values, to_categorical(linY.values), nb_epoch=NUM_SESS, batch_size=OPT_NN_BATCHSIZE, verbose=0, validation_split=RATIO_VALIDATION, shuffle=True)
    print model_ann.metrics, model_ann.metrics_names
    tic()

    clf = dict()
    clf['ann'] = model_ann
    clf['forest'] = model_forest
    clf['svm'] = model_one
    clf['nb'] = model_nb
    clf['log'] = model_logistic
    clf['sgd'] = model_sgd
    return clf

def phasePredict(classifiers):
    '''
    ======================
    PREDICTING
    ======================
    '''

    testx = pandas.read_csv(TEST_FILE, dtype=dtypes, index_col='id', verbose=0)
    testx.drop('color', axis=1, inplace=True)
    print "Predicting on ", testx.shape
    pred_ann = classifiers['ann'].predict_classes(testx.values, verbose=0)
    pred_forest = classifiers['forest'].predict(testx)
    pred_one = classifiers['svm'].predict(testx)
    pred_logi = classifiers['log'].predict(testx)
    pred_nb = classifiers['nb'].predict(testx)
    pred_sgd = classifiers['sgd'].predict(testx)

    #dfP = DataFrame(pred_ann + pred_forest + pred_one + pred_logi)
    #dfStr = dfP.apply(lambda x: hot2string(x, valid_labels), axis=1)

    dfP = DataFrame([pred_forest, pred_sgd, pred_one, pred_logi, pred_logi, pred_logi, pred_ann, pred_nb]).T
    m = dfP.mode(axis=1)
    dfP['mode'] = m[0] #Because there could be 'multiple' modes, select the first mode
    print dfP
    dfP = dfP.astype(int)
    dfStr = dfP['mode'].apply(lambda x: int2str(x))

    dfOut = DataFrame()

    dfOut['type'] = dfStr
    dfOut.set_index(testx.index.values, inplace=True)
    dfOut.to_csv(OUTPUT, index_label='id', index=True, columns=['type'])


def phaseSummary():
    print '\n=========SUMMARY======'
    print 'OPT_CHUNKSIZE ', OPT_CHUNKSIZE
    print 'NUM_LAYERS ', NUM_LAYERS
    print 'NUM_NEURONS ', NUM_NEURONS
    print 'NUM_SESS ', NUM_SESS
    print 'RATIO_DROP ', RATIO_DROP
    print 'OPT_NN_BATCHSIZE ', OPT_NN_BATCHSIZE
    print 'OPT_ACTIVATION ', OPT_ACTIVATION
    print 'RATIO_VALIDATION ', RATIO_VALIDATION



def main():
    c = phaseLearn()
    tic()
    phasePredict(c)
    tic()
    phaseSummary()
    tic()

if __name__ == "__main__":
    main()