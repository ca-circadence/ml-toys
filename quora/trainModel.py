'''
Written for quora machine-learning competition

'''
import pandas as pd
import keras
from pandas import DataFrame
import time
from fuzzywuzzy import fuzz
import numpy as np
import math
from nltk import ngrams
from nltk.corpus import brown as corpus_brown
from keras.callbacks import EarlyStopping
from keras.layers import Dense, Dropout
from keras.layers.noise import GaussianNoise
from keras.models import Sequential
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
import spacy
from spacy.tokens.doc import Doc


ROW_LIMIT = None
CLASS = 'is_duplicate'

training_file = '/home/ca/datasets/quora/train.csv'
test_file = '/home/ca/datasets/quora/test.csv'


train_prep_file = 'prepared-train.csv'
test_prep_file = 'prepared-test.csv'

timestamp = time.time()

submission_file = 'submission-{0}.csv'.format(timestamp)
MODEL_FILE = 'qann.h5'.format(timestamp)

glove_vector_size = 600

NUM_FEATURES = 5
FEATURE_SCHEMA = [('f' + str(i)) for i in range(1, NUM_FEATURES+1)] #Creates a list like ['f1', 'f2', 'f3'.... fn]
FEATURE_SCHEMA += [('g' + str(i)) for i in range(1, (glove_vector_size)+1)]

nlp = spacy.load('en')

def main():
    prepareData()
    #===============================
    #trainingPhase()
    #===============================
    predictPhase()


"""
Index(['id', 'qid1', 'qid2', 'question1', 'question2', 'is_duplicate'], dtype='object')
"""

def getFeatureVector(single):
    """
    Takes an input DataFrame and converts it into an expanded feature vector
    :param inputFrame:
    :type inputFrame: DataFrame
    :rtype: DataFrame
    :return:
    """
    global nlp



    schema = ['id'] + FEATURE_SCHEMA
    id = -1
    try:
        id = single['id']
    except KeyError as err:
        id = single['test_id']

    if id == -1:
        print("id should not be -1")
        exit()


    #Create a 'zero' vector that will be used if something goes wrong with parsing
    #Glove vectors are 300 large, so 600 accounts for 2 of them
    togeth = [0] * NUM_FEATURES + [0] * glove_vector_size
    vec = pd.Series([id] + (togeth), index=schema)
    feats = list()
    try:
        left = single['question1']
        right = single['question2']


        # SPlit the question up into multiple statements
        left_words = nlp(left)
        left_simple = " ".join(simplify(left_words))
        left_lemmas = nlp(left_simple)
        left_vec = getAbstract(left_lemmas)

        right_words = nlp(right)
        right_simple = " ".join(simplify(right_words))
        right_lemmas = nlp(right_simple)
        right_vec = getAbstract(right_lemmas)

        """
            FEATURE ENGINEERING
        """
        #Number of things 'appended' here needs to be the same size as NUM_FEATURES
        feats.append(fuzz.partial_ratio(left, right))
        feats.append(fuzz.token_set_ratio(left, right))
        feats.append(fuzz.token_set_ratio(left_simple, right_simple))
        feats.append(fuzz.token_sort_ratio(left, right))

        feats.append(left_lemmas.similarity(right_lemmas))
        assert ( len(feats) == NUM_FEATURES )

        #TODO: use previous vector defined and simply set values instead of new construct
        vec = pd.Series([id] + feats + list(left_vec) + list(right_vec), index=schema) #Concatenate the features
    except TypeError as err:
        print ("Error type {3} making feature vector: {0}, {1!s} / {2!s} ".format(id, left, right, type(err)))
    return vec

def simplify(words):
    """
    Replaces complex words with simpler words (lemmas)
    :param words:
    :type words: Doc
    :return:
    """
    # Lemmas are a generic form of the word

    stems = list()
    #TODO: This could be cleaner by using zip(*) and for i,j
    for token in words:
        stems.append(token.lemma_)

    return stems

def tic(comment):
    #Homemade version of matlab tic and toc functions
    import time
    global startTime_for_tictoc
    startTime_for_tictoc = time.time()
    print("Starting ", comment)

def toc():
    import time
    if 'startTime_for_tictoc' in globals():
        print("Elapsed: " + str(time.time() - startTime_for_tictoc) + " seconds.")
    else:
        print("Toc: start time not set")


def getAbstract(sentence):
    """
    converts the sentence to vector using word2vec
    :param sentence
    :type sentence: Doc
    :return:
    """
    return sentence.vector

def getBag(sentence):
    """

    :param sentence:
     :type sentence: str
     :rtype: set
    :return:
    """
    tmp = set()
    return tmp

def saveSubmission(output):
    """
    :param output:
    :type output: DataFrame
    :return:
    """
    output.to_csv(submission_file, columns=['id', CLASS], header=['test_id', CLASS], index=False, float_format='%.3f')


def getTestData():
    data = pd.read_csv(test_file, nrows=ROW_LIMIT)
    return data

def getTrainData():
    data = pd.read_csv(training_file, nrows=ROW_LIMIT)
    return data

def trainNN(X,Y):
    """
    :param X:
    :type X: np.array
    :param Y:
    :type Y: np.array
    :return: Sequential
    """
    x_train, x_val, y_train, y_val = train_test_split(X, Y, test_size=0.10, random_state=50)

    print("Training Keras ANN")
    width = X.shape[1]

    ann_model = Sequential()
    ann_model.add(Dense(width, input_dim=width, kernel_initializer='uniform', activation='relu'))
    ann_model.add(Dropout(0.10))
    ann_model.add(Dense(1, activation='softmax'))  # sigmoid or softmax

    # Compile model
    print("Compiling Model")
    ann_model.compile(loss='binary_crossentropy', optimizer='adagrad', metrics=['accuracy'])
    early_stopping = EarlyStopping(monitor='val_loss', patience=50, min_delta=0.001)

    #Create log files for the tensorboard server (visualization)
    tensor_vis = keras.callbacks.TensorBoard(log_dir='logs', histogram_freq=0, write_graph=True, write_images=True)

    print("Training the ANN")
    ann_model.fit(x_train, y_train, epochs=100, batch_size=200, validation_split=0.10, shuffle=True, verbose=1, callbacks=[early_stopping])

    print("Evaluating the models")
    # evaluate the model
    scores = ann_model.evaluate(x_val, y_val)
    print("\n %s: %.2f%%, %s: %.2f " % (ann_model.metrics_names[1], scores[1]*100, ann_model.metrics_names[0], scores[0]))

    return ann_model

def preProcess(data, key):
    """
    :param data:
    :type data: DataFrame
    :return:
    :rtype DataFrame
    """
    print("Creating feature vectors")
    temp = DataFrame(data, columns=FEATURE_SCHEMA + list(data.columns))
    features = temp.apply(lambda row: getFeatureVector(row), axis=1)

    print("Merging Features")
    features_only = features[0:len(FEATURE_SCHEMA)]

    return features_only

def xysplit(data):
    """
    Splits a DataFrame into X,Y matrices where X is m number of n features as a matrix m x n and Y is the target class vector
    :param data:
    :type data: DataFrame
    :return: x,y
    :rtype: DataFrame, DataFrame
    """
    d = None
    target = None

    try:
        target = data[CLASS]
        d = data.drop(CLASS, axis=1)
        d = d.drop('id', axis=1)
        d = d / 100.0
    except Exception as err:
        print("Error splitting data")

    return d, target

def makePredictions(nueral_network, x):
    """
    :param x: numpy array of vectors (X)
    :type x:
    :return:
    """
    p = nueral_network.predict_proba(x, verbose=1)
    results = DataFrame(p, columns=[CLASS])
    return results

def prepareData():
    tic('Preparing data')
    # Read the training data
    tic('preprocess')
    train_original = getTrainData()
    test_original = getTestData()

    # Prepare the data for training
    prepTrain = preProcess(train_original, 'id')  # DF returned
    prepTest = preProcess(test_original, 'test_id')  # DF returned

    # save the prepped data to a pickle
    schema = FEATURE_SCHEMA + [CLASS]
    prepTrain[CLASS] = train_original[CLASS]
    prepTrain.to_csv(train_prep_file, columns=schema, index_label='id')
    prepTest.to_csv(test_prep_file, columns=schema, index_label='id')
    toc()

def trainingPhase():
    prep = pd.read_csv(train_prep_file, nrows=ROW_LIMIT)
    x,y = xysplit(prep)

    #Train the model on prepped data
    tic('training')
    model = trainNN(x.values,y.values)

    print("Saving the ANN")
    model.save(MODEL_FILE)
    toc()

def predictPhase():
    #Read the test data
    tic('read test')
    test = pd.read_csv(test_prep_file, nrows=ROW_LIMIT)
    x_test, y_test = xysplit(test)
    toc()

    model = keras.models.load_model(MODEL_FILE)
    tic('predict')
    #Predict on test data using the model
    predictions = makePredictions(model, x_test.values)
    toc()

    #Output the predictions
    test[CLASS] = predictions
    saveSubmission(test)


if __name__ == "__main__":
    main()
