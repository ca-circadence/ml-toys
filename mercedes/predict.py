
import pandas as pd
import time
import math
from sklearn.externals import joblib
from pandas import DataFrame
import keras
import common_merc as cm
readlimit = None

(low, mid, high, col_names) = joblib.load('train_info.pkl')

print (col_names)

testdata = pd.read_csv('modified_test.csv', index_col='ID', nrows=readlimit)

#For testing purposes
if 'y' in testdata.columns:
    print "dropping y"
    testdata.drop('y', axis=1, inplace=True)


for c in col_names:
    if c not in testdata and c != 'y':
        print c, " not present in test data"
        testdata[c] = 0


#Load the ANN
ann = keras.models.load_model('network.ann')

p = ann.predict(testdata.values, batch_size=32, verbose=0)
i = testdata.index

preds = p

#Because I don't know a conveiniant way to set the value of the dataframe in the constructor
output = DataFrame(None, columns=['ID', 'y'])
output['ID'] = i
output['y'] = preds

output.to_csv('submission.csv', index=False )
