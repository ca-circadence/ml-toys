# -*- coding: utf-8 -*-
"""
Created on Wed Aug 26 11:51:28 2016

@author: Christopher Aprea
@company Circadence
"""


from keras.models import Sequential
from keras.layers import Dense, Activation
from keras.optimizers import SGD

import random as RN

filepath = 'iris.data'
#outputFolder = 'iris_images'


MAX_SAMPLES = 9999999
NUMFEATURES = 4


'''
    Operate on a single line at once, because we could get
    out of memory errors if trying to convert an exceedingly large
    csv file
'''


def getData():
    with open(filepath, 'r') as dfile:
        count = 0
        dataFeatures = []
        dataLabels = []
        for line in dfile:
            count = count + 1
            content = line.split(',')
            if (len(content) > NUMFEATURES):
                strValues = content[0:NUMFEATURES]
                features = map(float, strValues)
                targetClass = str(content[-1]).strip()  # Get label


                dataFeatures.append(features)
                dataLabels.append(targetClass)
    dfile.close()
    return dataFeatures, dataLabels

def main():
    X_train, Y_train = getData()
    net = makeModel()
    train(net, X_train, Y_train)



def makeModel():

    model = Sequential()
    model.add(Dense(4, input_dim=NUMFEATURES, init='uniform', activation='relu'))
    model.add(Dense(3, init='uniform', activation='relu'))
    model.add(Dense(3, init='uniform', activation='sigmoid'))

    #Compile model
    model.compile(loss='categorical_crossentropy', optimizer='sgd', metrics=['accuracy'])

    #Loss fucntion
    #model.compile(loss='categorical_crossentropy', optimizer=SGD(lr=0.01, momentum=0.9, nesterov=True))

    return model

def train(model, X, Y):
    model.fit(X, Y, nb_epoch=30, batch_size=10)

    # evaluate the model
    scores = model.evaluate(X, Y)
    print("%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))





if __name__ == "__main__":
    main()