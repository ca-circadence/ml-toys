import time
import math
import pandas as pd
from keras.callbacks import EarlyStopping
from keras.layers import Dense, Dropout
from keras.layers.noise import GaussianNoise
from keras.models import Sequential
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import train_test_split

import common_merc as cm

readlimit = None


train = pd.read_csv('modified_train.csv', index_col='ID', nrows=readlimit)


YS = train.pop('y')

x_train, x_val, y_train, y_val = train_test_split(train, YS, test_size=0.005, random_state=50)

print "Validation size: ", x_val.shape

width = train.shape[1]

ann = Sequential()
ann.add(Dense(width, activation='linear', input_dim=width))

ann.add(Dense(width * 5, activation='tanh'))

ann.add(Dense(1, activation='linear')) #sigmoid/softmax
ann.compile(loss='mae', optimizer='sgd', metrics=['mse', 'accuracy'])


early_stopping = EarlyStopping(monitor='loss', patience=3, min_delta=0.001)
ann.fit(x_train.values, y_train.values, epochs=1000, batch_size=300, validation_split=0.15, shuffle=True, verbose=1, callbacks=[early_stopping])

print "Done fitting"
scores = ann.evaluate(x_val.values, y_val.values)
# evaluate the model

ann.save('network.ann', overwrite=True)
print("\n %s: %.4f, %s: %.4f, %s: %.4f%%" % (ann.metrics_names[0], scores[0], ann.metrics_names[1], scores[1], ann.metrics_names[2], scores[2]))
