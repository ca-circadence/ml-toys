
"""
@author Christopher Aprea
@date September 2016
"""
import csv
import numpy
import pandas as PD
from pandas import DataFrame
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten, GaussianNoise
from keras.models import load_model
import random as RANDOM
from keras.optimizers import SGD
import keras.utils.np_utils as KUTIL
import time as TIME
import keras
from keras.callbacks import EarlyStopping

#test comment

SESS = 5000


FORGETNESS = 0.5
MUTATION_CHANCE = 0.10


TRAIN_FILE = 'leaf-train.csv'
TEST_FILE = 'leaf-test.csv'
MODEL_FILE = 'model.nn'

def getData(dataFile):
    #Read the input as a Pandas DataFrame
    data = PD.read_csv(dataFile)

    print "Keys in Data: ", data.keys()

    labels = None
    if 'species' in data.keys():
        labels = data.pop('species')

    if 'id' in data.keys():
        ids = data.pop('id')

    #NORMALIZED
    data = data.apply(lambda x: (x - x.mean()) / (x.max() - x.min()))

    return ids, data, labels

def createFF(numInput, numOutput):
    model = Sequential()

    w = int(numInput * 1.1)
    model.add(Dense(w, input_dim=numInput, init='uniform', activation='relu'))

    model.add(GaussianNoise(0.1))


    model.add(Dense(numOutput, init='uniform', activation='softmax'))


    #Compile model
    sgd = SGD(lr=0.1, decay=1e-6, momentum=0.9, nesterov=True)

    model.compile(loss='mean_squared_error', optimizer='nadam') #rmsprop

    return model

def hotencode(arr):
    '''One hot encode a numpy array of objects (e.g. strings)'''
    uniques, ids = numpy.unique(arr, return_inverse=True)
    return KUTIL.to_categorical(ids, len(uniques))


def trainingPhase():
    ids, train_x, train_y = getData(TRAIN_FILE)

    print "Number of unique labels: ", train_y.nunique()

    hot_y = hotencode(train_y.values)

    model = createFF(train_x.shape[1], hot_y.shape[1])

    early_stopping = EarlyStopping(monitor='val_loss', patience=1500)
    model.fit(train_x.values, hot_y, validation_split=0.4, batch_size=50, nb_epoch=SESS, callbacks=[early_stopping])

    model.save(MODEL_FILE)


    print "Done training"


def predictionPhase():
    model = load_model(MODEL_FILE)
    test_ids, test_x, test_y = getData(TEST_FILE)


    predictions = model.predict_proba(test_x.values, verbose=1)

    output = PD.concat([test_ids, DataFrame(predictions)], axis=1)
    names = ['id','Acer_Capillipes','Acer_Circinatum','Acer_Mono','Acer_Opalus','Acer_Palmatum','Acer_Pictum','Acer_Platanoids','Acer_Rubrum','Acer_Rufinerve','Acer_Saccharinum','Alnus_Cordata','Alnus_Maximowiczii','Alnus_Rubra','Alnus_Sieboldiana','Alnus_Viridis','Arundinaria_Simonii','Betula_Austrosinensis','Betula_Pendula','Callicarpa_Bodinieri','Castanea_Sativa','Celtis_Koraiensis','Cercis_Siliquastrum','Cornus_Chinensis','Cornus_Controversa','Cornus_Macrophylla','Cotinus_Coggygria','Crataegus_Monogyna','Cytisus_Battandieri','Eucalyptus_Glaucescens','Eucalyptus_Neglecta','Eucalyptus_Urnigera','Fagus_Sylvatica','Ginkgo_Biloba','Ilex_Aquifolium','Ilex_Cornuta','Liquidambar_Styraciflua','Liriodendron_Tulipifera','Lithocarpus_Cleistocarpus','Lithocarpus_Edulis','Magnolia_Heptapeta','Magnolia_Salicifolia','Morus_Nigra','Olea_Europaea','Phildelphus','Populus_Adenopoda','Populus_Grandidentata','Populus_Nigra','Prunus_Avium','Prunus_X_Shmittii','Pterocarya_Stenoptera','Quercus_Afares','Quercus_Agrifolia','Quercus_Alnifolia','Quercus_Brantii','Quercus_Canariensis','Quercus_Castaneifolia','Quercus_Cerris','Quercus_Chrysolepis','Quercus_Coccifera','Quercus_Coccinea','Quercus_Crassifolia','Quercus_Crassipes','Quercus_Dolicholepis','Quercus_Ellipsoidalis','Quercus_Greggii','Quercus_Hartwissiana','Quercus_Ilex','Quercus_Imbricaria','Quercus_Infectoria_sub','Quercus_Kewensis','Quercus_Nigra','Quercus_Palustris','Quercus_Phellos','Quercus_Phillyraeoides','Quercus_Pontica','Quercus_Pubescens','Quercus_Pyrenaica','Quercus_Rhysophylla','Quercus_Rubra','Quercus_Semecarpifolia','Quercus_Shumardii','Quercus_Suber','Quercus_Texana','Quercus_Trojana','Quercus_Variabilis','Quercus_Vulcanica','Quercus_x_Hispanica','Quercus_x_Turneri','Rhododendron_x_Russellianum','Salix_Fragilis','Salix_Intergra','Sorbus_Aria','Tilia_Oliveri','Tilia_Platyphyllos','Tilia_Tomentosa','Ulmus_Bergmanniana','Viburnum_Tinus','Viburnum_x_Rhytidophylloides','Zelkova_Serrata']

    assert isinstance(output, DataFrame)

    output.to_csv('submit.csv', header=names, index=False)
    print "\nDone with results"


def main():
    trainingPhase()
    predictionPhase()
    pass


if __name__ == "__main__":
    main()

