# Create first network with Keras
import numpy
from keras.layers import Dense
from keras.models import Sequential
from sklearn.model_selection import train_test_split

# fix random seed for reproducibility
seed = 7
numpy.random.seed(seed)
# load pima indians dataset
dataset = numpy.loadtxt("pima-indians-diabetes.data", delimiter=",")
# split into input (X) and output (Y) variables




X = dataset[:,0:8]
Y = dataset[:,8]

X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.15, random_state=0)

# create model
model = Sequential()
model.add(Dense(12, input_dim=8, init='uniform', activation='relu'))
model.add(Dense(8, init='uniform', activation='relu'))
model.add(Dense(1, init='uniform', activation='sigmoid'))
# Compile model
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
# Fit the model
model.fit(X_train, y_train, nb_epoch=150, batch_size=10, validation_data=(X_test, y_test))
# evaluate the model
scores = model.evaluate(X_test, y_test)
print "\n"
print scores
print("\n %s: %.2f%% " % (model.metrics_names[1], scores[1]*100))