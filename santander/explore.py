
from sklearn.decomposition import PCA

import pandas as PD
from pandas import DataFrame
import dataparser

DATA_PATH = '/home/chris/datasets/santander/'
TRAIN1 = DATA_PATH + 'train.sample.csv'



BATCHSIZE = 50000

#engine = create_engine('postgresql://postgres:4thRock@10.20.0.143:5432/chris')


dftrainraw = PD.read_csv(TRAIN1, chunksize=BATCHSIZE, dtype=str, index_col='ncodpers')

c = 0
alldata = DataFrame()
#Iterate over each piece of the data set
for part in dftrainraw:
    c = c + 1
    #x = dataparser.sanitize(part)
    print  "Chunk ", c, " ", part.shape
    alldata = PD.concat([alldata, part], ignore_index=True)


print "Sanitizing"
print alldata.shape
alldata = dataparser.sanitize(alldata)
print 'Normalizing'
alldata = dataparser.normalize(alldata)
dataparser.forceshape(alldata)
print "Splitting the data"
x, y = dataparser.splitXY(alldata)
print "Final input feature space", x.shape
print x.columns
