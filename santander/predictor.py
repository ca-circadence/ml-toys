import pickle
import time

import pandas as PD
from keras.models import Sequential
from pandas import DataFrame
from sklearn.ensemble import RandomForestClassifier

import dataparser
import util

# Getting back the objects:
with open(util.STOREFILE) as f:  # Python 3: open(..., 'rb')
    ann, forest, class_names = pickle.load(f)

# ===========================
# Prediction phase
# =============================
print "Reading in test data / ", time.time()
df = PD.read_csv(util.TEST_FILE, index_col='ncodpers', dtype=str)
testx, dummy = dataparser.splitXY(df)
testx = dataparser.processChunk(testx)
testx = dataparser.postprocess(testx)
testx = dataparser.multishape(testx, dataparser.KEYS_FULL)
#testx = dataparser.normalize(testx)

print "Predicting", testx.shape, " / ", time.time()
s = util.tic()
assert isinstance(ann, Sequential)
assert isinstance(forest, RandomForestClassifier)

pred_ann = ann.predict_proba(testx.values, verbose=1)
pred_forest = forest.predict(testx.values )
print "Predicting on took ", util.toc(s)

with open('preds.pickle', 'r') as f:
    pred_ann, pred_forest = pickle.load(f)

print "Preparing output"
combined_predictions = (pred_ann + pred_forest)
combined_predictions /= 2.0

preds = DataFrame(combined_predictions, index=testx.index)
binrep = (preds > 0.5).astype(bool)


def getlbls(dfrow):
    matches = []
    c = 0
    for value in dfrow:
        if value:
            matches.append(class_names[c])
        c += 1
    line = ' '.join(matches)
    return line


print "Converting to string labels"
strpre = list()
for idx, row in binrep.iterrows():
    strpre.append(getlbls(row.values))

testx['added_products'] = strpre

print "Saving"

testx.to_csv('predictions.csv', index_label='ncodpers', columns=['added_products'])

print "Ready/Done"
