
import pandas as pd
import keras
from pandas import DataFrame
import time

import nltk
from fuzzywuzzy import fuzz
import numpy as np
import math
from nltk import ngrams
from nltk.corpus import brown as corpus_brown

ROW_LIMIT = 10000

training_file = '/home/ca/datasets/quora/train.csv'



def fuzzer(singleRow):
    num = 0
    try:
        num = fuzz.partial_ratio(singleRow['question1'],singleRow['question2'])
    except:
        "do nothing"
    return num

start = time.time()

data = pd.read_csv(training_file, nrows=ROW_LIMIT)


task_1_start= time.time()


data['test1'] = data.apply(lambda row: fuzzer(row), axis=1)
task_1_end = time.time()
elapsed1 = task_1_end - task_1_start
print (elapsed1)

task_2_start= time.time()
for r, row in data.iterrows():
    data.ix['test2'] = fuzzer(row)
    elapsed2 = time.time() - task_2_start
    if ( elapsed2 > (elapsed1 * 5)):
        print ("giving up", elapsed2, r)
        break

elapsed2 = time.time() - task_2_start


print (elapsed1, elapsed2)


print(data.head())

