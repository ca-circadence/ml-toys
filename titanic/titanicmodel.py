
"""
@author Christopher Aprea
@date September 2016
"""
import csv
import numpy
import pandas as PD
from pandas import DataFrame
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.models import load_model
from keras.optimizers import SGD
import time as TIME

#test comment

KEYS_ENCODED = ['Embarked', 'Name', 'Cabin']

KEYS_IGNORED = ['Fare','PassengerId','Ticket','Parch','SibSp']
KEYS_NORMED = ['Age','Pclass']
SESS = 2000
LAYERS = 5
LAYER_SIZE = 25
SAMPLE_SIZE = 0.50
FORGETNESS = 0.5


def getData(dataFile):
    #Read the input as a Pandas DataFrame
    data = PD.read_csv(dataFile)
    return data


def cleanData(data):
    """
    @type data: DataFrame
    @param data:
    @return: features, classLabel
    @rtype: DataFrame
    """
    assert isinstance(data, DataFrame)
    #Clean up the genders, convert from str to [0,1]
    data.replace('male', 1, inplace=True)
    data.replace('female', 0, inplace=True)


    #Get the survival label
    survived = None
    try:
        survived = data.pop('Survived')
    except:
        print "Labeled field not found"


    #RegExp matches for Miss.|Mrs.|Don.|etc..
    expr = '(.+,\s)([\s,A-Z,a-z]+)(\..*)'
    data['Name'].replace(expr, value='\\2', regex=True, inplace=True)
    expr = '(Dona|Lady|Mlle|Mme|the Countess)+'
    data['Name'].replace(expr, value='Lady', regex=True, inplace=True)
    expr = '(Capt|Col|Don|Major|Master|Sir|Jonkheer)+'
    data['Name'].replace(expr, value='Sir', regex=True, inplace=True)
    expr= '([A-Z])+\d*(.*)+'
    data['Cabin'].replace(expr, value='\\1', regex=True, inplace=True)



    #Add the 'family size' feature
    fam = DataFrame(data['SibSp'] + data['Parch'], columns=['family'])
    fam.fillna(0, inplace=True)
    data = PD.concat([data, fam], axis=1)


    # Remove the passengerid, and name columns because they are not useful and not easily encoded
    data.drop(KEYS_IGNORED, axis=1, inplace=True)

    # One-Hot Encode the Embarked location
    print "Hot-Encoding and Filling Modes..."
    print data[KEYS_ENCODED].mode()
    data[KEYS_ENCODED].fillna(data[KEYS_ENCODED].mode(), inplace=True)
    hotcodes = PD.get_dummies(data[KEYS_ENCODED])
    data = PD.concat([data, hotcodes], axis=1)
    data.drop(KEYS_ENCODED, axis=1, inplace=True)



    # replace NaN with average value
    avgs = data.mean()
    data = data[avgs.keys()].fillna(avgs, inplace=True)


    # Assuming same lines from your example
    #data[KEYS_NORMED] = data[KEYS_NORMED].apply(lambda x: (x - x.mean()) / (x.max() - x.min()))


    #final data

    print data.keys()
    return data, survived



def createModelFF(dimension):

    func = 'linear'
    start = 'uniform'

    model = Sequential()
    model.add(Dense(25, input_dim=dimension, init=start, activation='linear'))

    model.add(Dense(LAYER_SIZE, init=start, activation=func))
    model.add(Dropout(FORGETNESS))

    model.add(Dense(1, init=start, activation='linear'))
    #model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    model.compile(loss='mean_squared_error', optimizer='rmsprop', metrics=['accuracy'])
    #print(model.summary())
    return model



def train(model, trainx, trainy, valx, valy):



    trainX = trainx
    trainY = trainy


    print "X size ", trainX.shape, " Y size ", trainY.shape
    print "Training the model..."

    TIME.sleep(0.1)
    model.fit(trainX, trainY, batch_size=800, nb_epoch=SESS, verbose=1, validation_data=(valx, valy))
    TIME.sleep(0.1)

    score = model.evaluate(valx, valy, verbose=1)

    #model.save_weights('titanic.nn')
    model.save('titanic.nn')
    print('Test score:', score[0])
    print('Test accuracy:', score[1])


def main():
    print "Program started"
    learning()
    predictions()

def learning():
    training = getData('train.csv')

    dfX, dfY = cleanData(training)

    # Get a random sample of 80% of the data, save the holdout as validation
    tX = dfX.sample(frac=SAMPLE_SIZE, random_state=5)
    vX = dfX.drop(tX.index)  # validiation X is ~trainX

    tY = dfY[tX.index]  # train Y is same indexs as trainX
    vY = dfY.drop(tX.index)  # validation Y is ~trainX

    model = createModelFF(dfX.shape[1])
    #train(model, tX.values, tY.values, vX.values, vY.values)


def predictions():
    """
            Batch Prediction
    """
    model = load_model('titanic.nn')

    print "Program ended"
    test = getData('test.csv')
    pids = test['PassengerId'].values

    dfX, dfY = cleanData(test)

    y = model.predict_classes(dfX.values, verbose=1)
    assert isinstance(y, numpy.ndarray)

    pids = pids.reshape(y.shape)

    output = numpy.hstack( (pids, y) )

    numpy.savetxt('output.csv', output, fmt='%i', header='PassengerId,Survived', delimiter=',', comments='')

    print "Done with ", y.size, " results"


if "__main__" == __name__:
    main()

