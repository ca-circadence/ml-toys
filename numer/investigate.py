import pandas as pd
import seaborn as sns
from pandas import DataFrame
from sklearn import decomposition

'''
CONFIGURATION
'''

FILE_TRAIN = '/home/chris/datasets/numer_datasets/numerai_training_data.csv'
EPOCHS = 3
ROWS = 2000000

# Read the data
print("Loading training data ", FILE_TRAIN)
# Load the data from the CSV files
training_data = pd.read_csv(FILE_TRAIN, header=0)

print("Preparing training data")
rawY = training_data['target']
rawX = training_data.drop('target', axis=1)

y = DataFrame(rawY)

print "PCA on ", rawX.shape
scores = {}
pca = decomposition.PCA(n_components=45)
x = DataFrame(pca.fit_transform(rawX))
scores = pca.explained_variance_

combined = pd.concat([x, y], axis=1, ignore_index=True)
sns.set(style="ticks", color_codes=True)

g = sns.plt.plot(scores, color='red')
gg = sns.plt.plot(pca.explained_variance_ratio_, color='blue')
sns.plt.savefig('pca.png')
print "DONE"
