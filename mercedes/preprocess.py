
from sklearn.preprocessing import MultiLabelBinarizer
import pandas as pd
from pandas import DataFrame
import common_merc as merc
from sklearn.externals import joblib
import numpy as np

"""
Read in the train file
Read in the test file

for every symbolic field
    get the unique values in test, append the unique values in train
    fit transform the field

"""


readlimit = None

primer_train = pd.read_csv('train.csv', index_col='ID', nrows=readlimit)
primer_test = pd.read_csv('test.csv', index_col='ID', nrows=readlimit)


print "Train Original ", primer_train.shape
print "Test Original: ", primer_test.shape

mtrain = merc.transform(primer_train)
low, high, mid = mtrain['y'].min(), mtrain['y'].max(), mtrain['y'].mean()
#Normalize the Y variable
#mtrain['y'] = merc.normalize(mtrain['y'])

mtest = merc.transform(primer_test)

print "Transformed Train/Test: ", mtrain.shape, " / ", mtest.shape

merc.selective_omit(mtest, mtrain.columns)

print "Omitted colrumns, resulting shape: ", mtrain.shape, " / ", mtest.shape

print "Saving modifed to file"
print "Low: ", low, " Mid: ", mid, " High: ", high
joblib.dump((low, mid, high, mtrain.columns),'train_info.pkl')

mtrain.to_csv('modified_train.csv')
mtest.to_csv('modified_test.csv')
