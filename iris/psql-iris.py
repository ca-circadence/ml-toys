

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy
import pandas as PD
from pandas import DataFrame

from sklearn.cluster import KMeans
from sklearn import datasets
import sqlalchemy
from sqlalchemy import create_engine

'''
The purpose of this script is to proof of concept for using SQL Alchemy and Pandas

'''


raw = PD.read_csv('iris.data', names=['m1', 'm2', 'm3', 'm4', 'lbl'])
dfy = raw.pop('lbl')
dfx = raw


engine = create_engine('postgresql://postgres:4thRock@10.20.0.143:5432/chris')
dfx.to_sql('iris', engine, if_exists='replace')



output = PD.read_sql('iris', engine, index_col='index')

print output
print dfx.shape, output.shape
print "Are they the same?", numpy.array_equal(dfx, output)